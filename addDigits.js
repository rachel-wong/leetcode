// Given a non-negative integer num, repeatedly add all its digits until the result has only one digit.

var addDigits = function (num) {
  while (num.toString().length != 1) {
    num = num.toString().split("").map(Number).reduce((a,b) => {
      return a += b
    })
    addDigits(num)
  }
  return num
}

console.log(addDigits(38)) //=> 2

// other solutions
var addDigits = function (num) {
  if (isNaN(num) || num === 0) return 0;
  if (num < 10) return num;
  return num % 9 === 0 ? 9 : num % 9;
};


var addDigits = function (num) {
  var sum = 0, temp;
  while (num) {
    temp = num % 10;
    sum += sum + temp >= 10 ? temp - 9 : temp;
    num = parseInt(num / 10);
  }
  return sum;
};

var addDigits = function (num) {
  return (num - 1) % 9 + 1;
};