// Complete the function that accepts a string parameter, and reverses each word in the string.All spaces in the string should be retained.

// "This is an example!" == > "sihT si na !elpmaxe"
// "double  spaces" == > "elbuod  secaps"

function reverseWords(str) {
  let check = str.split(" ")

  for (i = 0; i < check.length; i++) {
    check[i] = check[i].split("").reverse().join("")
  }
  return check.join(" ")
}

console.log(reverseWords('The quick brown fox jumps over the lazy dog.'))

// other solutions
function reverseWords(str) {
  return str.split(' ').map(function (word) {
    return word.split('').reverse().join('');
  }).join(' ');
}