// Given a string S, return the "reversed" string where all characters that are not a letter stay in the same place, and all letters reverse their positions.

// S.length <= 100
// 33 <= S[i].ASCIIcode <= 122 
// S doesn't contain \ or "

// 1) Create a temporary character array say temp[].
// 2) Copy alphabetic characters from given array to temp[].
// 3) Reverse temp[] using standard string reversal algorithm.
// 4) Now traverse input string and temp in a single loop.Wherever there is alphabetic character is input string, replace it with current character of temp[].

var reverseOnlyLetters = function (string) {
  let isAlphabetical = /^[a-zA-Z]+$/
  let temp = [] // collect all the alphabets
  string = string.split("")
  string.forEach(item => {
    if (isAlphabetical.test(item) == true) {
      temp.push(item)
    } else {
      return item
    }
  })
  temp = temp.reverse()
  let i = 0
  while (i < temp.length) {
    for (let y = 0; y < string.length; y++) {
      if (isAlphabetical.test(string[y]) == true) {
        string[y] = temp[i]
        i++ // move forward in temp only when an alphabet has been replaced in string
      } else {
        string[y] = string[y] // don't move forward when it's not an alphabet in string
      }
    }
  }
  return string.join("")
}

console.log(reverseOnlyLetters("Test1ng-Leet=code-Q!")) // => "Qedo1ct-eeLg=ntse-T!"
console.log(reverseOnlyLetters("ab-cd")) // => "dc-ba"