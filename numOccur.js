  // Given an array of integers arr, write a function that returns true if and only if the number of occurrences of each value in the array is unique.

  // wrong understanding of question: it is not that each item appears once, but their appearance frequency itself is unique
  // function isOne(item) {
  //   return item === 1
  // }

  // var uniqueOccurrences = function (arr) {
  //   let count = {}
  //   arr.forEach(item => {
  //     count[item] = (count[item] + 1) || 1
  //   })
  //   let values = Object.values(count)
  //   return values.every(isOne) == true ? true : false
  // }


  var uniqueOccurrences = function (arr) {
    let count = {}
    arr.forEach(item => {
      count[item] = (count[item] + 1) || 1
    })
    let values = Object.values(count)
    return values.sort((a, b) => a > b).filter((item, i, arr) => arr.indexOf(item) !== i).length === 0 ? true : false
  }

  console.log(uniqueOccurrences([1, 2, 2, 1, 1, 3])) // => true
  console.log(uniqueOccurrences([1, 2])) //=> false

  // other solutions
  const uniqueOccurrences = arr => {
    let res = {}
    for (let i = 0; i < arr.length; i++) {
      res[arr[i]] = (res[arr[i]] || 0) + 1
    }
    const values = Object.values(res)
    return new Set(values).size === values.length // much better way to check if the array has only unique values (or values)
  }