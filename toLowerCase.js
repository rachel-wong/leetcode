var toLowerCase = (string) => {
  let isAlphabetical = /^[a-zA-Z]+$/
  return string.split("").map(item => {
    if (isAlphabetical.test(item) == true) {
      return item = item.toLowerCase()
    } else {
      return item
    }
  }).join("")
}

// other solutions
var toLowerCase = function (str) {
  return str.split('').reduce((prev, cur) => {
    let code = cur.charCodeAt(0);
    if (64 < code && 91 > code) {
      return prev + String.fromCharCode(32 + code);
    }
    return prev + cur;
  }, '');
};

console.log(toLowerCase("Hello")) //=> "hello"
console.log(toLowerCase("here")) //=> "here"
console.log(toLowerCase("LOVELY")) //=> "lovely"
console.log(toLowerCase("al&phaBET")) //=> "al&phabet"