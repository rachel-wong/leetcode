// Given two strings s and t , write a function to determine if t is an anagram of s.

// You may assume the string contains only lowercase alphabets.

var isAnagram = function (s, t) {
  s = s.split("").sort().join("")
  t = t.split("").sort().join("")
  if (s === t) {
    return true
  } else {
    return false
  }
}

console.log(isAnagram("anagram", "nagaram")) // => true
console.log(isAnagram("rat", "car")) //=> false