// Given an array A of non-negative integers, return an array consisting of all the even elements of A, followed by all the odd elements of A.

// You may return any answer array that satisfies this condition.

// 1 <= A.length <= 5000
// 0 <= A[i] <= 5000

var sortArrayByParity = function (input) {
  let even = []
  let odd = []
  input.forEach(item => {
    if (item % 2 === 0) {
      even.push(item)
    } else {
      odd.push(item)
    }
  })
  return even.concat(odd)
}

console.log(sortArraybyParity([3, 1, 2, 4])) //=> [2,4,3,1] or [4,2,3,1], [2,4,1,3], and [4,2,1,3]