// 1) You must not repeat the same key consecutively if there are more than one(the order of keys in the scroll is important!).
// Ex: to_leet('aaaa') # => '4@4@'
// 2) The input will consist only of lowercase alphabetical characters(a-z) and single spaces.
// Ex: to_leet('a a a a a a a') # => '4 @ 4 @ 4 @ 4'
// 3) If a key does not exist for a character, keep the character as is('m' is one such character without a key)
// Ex: to_leet('mama') # => 'm4m@'
// 4) The strings must represent the key(s) on the scroll, meaning that certain characters might have to be escaped.

function to_leet(string) {
  let scroll = {
    a: ['4', '@'],
    b: ['|3', '8'],
    d: ['|)', 'o|'],
    e: ['3'],
    f: ['|:'],
    g: ['9', '6'],
    h: ['|-|', ']-[', '}-{', '(-)', ')-(', '#'],
    i: ['1', '!', ']['],
    j: ['_|'],
    k: ['|<', '|{'],
    l: ['|_'],
    n: ['|\|'],
    o: ['0'],
    p: ['|2', '|D'],
    q: ['(,)'],
    r: ['|Z', '|?'],
    s: ['5', '$'],
    t: ['+', '7'],
    v: ['|/', '\/'],
    w: ['\^/', '//'],
    x: ['><', '}{'],
    y: ['`/'],
    z: ['(\)'],
  }
  string = string.split("")
  let result = []
  for (let i = 0; i < string.length; i++) {
    if (scroll[string[i]] != undefined) {
      let temp = scroll[string[i]].join("")
      result.push(temp)
    } else {
      result.push(string[i])
    }
  }
  console.log("result", result.join(""))
  return result.join("")
}

to_leet('aaaa')