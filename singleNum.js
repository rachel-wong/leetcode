// Given a non-empty array of integers, every element appears twice except for one. Find that single one.

var singleNumber = function (nums) {
  let count = {}
  for (item of nums) {
    count[item] = (count[item] || 0) + 1
  }
  // console.log(Object.keys(count))
  return Object.keys(count).forEach(key => {
    console.log(count[key])
    if (count[key] === 1) {
      return count[key]
    }
  })
}

console.log(singleNumber([2, 2, 1])) // => 1
console.log(singleNumber([4, 1, 2, 1, 2])) // => 4