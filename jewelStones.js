// You're given strings J representing the types of stones that are jewels, and S representing the stones you have.  Each character in S is a type of stone you have.  You want to know how many of the stones you have are also jewels.

// The letters in J are guaranteed distinct, and all characters in J and S are letters. Letters are case sensitive, so "a" is considered a different type of stone from "A".

var numJewelsInStones = function (j, s) {
  let counter = 0
  j = j.split("")
  s = s.split("").forEach(char => {
    if (j.includes(char)) counter++
  })
  return counter
}

console.log(numJewelsInStones("aA", "aAAbbbb")) //=> 3


// other solutions
var numJewelsInStones = function (J, S) {
  var regex = new RegExp(`[${J}]`, 'g');
  var res = S.match(regex) || [];

  return res.length;
};

const numJewelsInStones = (J, S) => S.split('').filter(char => J.indexOf(char) !== -1).length;